<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
        $this->fecha_cod = date('Ymd');
        
    }

    function index(){
        $c_t=$_GET['c_t'];
        $idc=$_GET['idc'];

        $data['c_t']=$c_t;;
        $data['idc']=$idc;;
        if(isset($_GET['idpersonal']) ){
            $idpersonal=$_GET['idpersonal'];
            $data['ms_cli']=0;
            $data['ms_per']=1;
        }else{
            $idpersonal=0;
            $data['ms_cli']=1;
            $data['ms_per']=0;
        }
        $data['idpersonal']=$idpersonal;

        if (isset($_GET['test'])) {
            $cadena=$_GET['test'];
        }else{
            $cadena='';
        }
        $tipo_dep='';
        //log_message('error','$cadena: '.$cadena);
        //$verificar = password_verify($this->fecha_cod,$cadena);
        //if($verificar==false){
          //  log_message('error','error con la cadena '.$this->fecha_cod.' '.$cadena);
            $verificar=true;
        //}
            $resutl = $this->ModeloCatalogos->getselectwheren('chat_ini',array('id'=>$idc,'codigol'=>$cadena));
            if($resutl->num_rows()>0){
                $verificar=true;
            }else{
                $verificar=false;
            }
        if($verificar){
            //======================================
                if($c_t==0){
                    $this->ModeloCatalogos->updateCatalogo('chat_ini',array('activo'=>0),array('id <'=>$idc,'codigo'=>$this->fecha_cod,'status'=>1,'tipo'=>0));
                }
                
                $idpersonal_inicio=0;
                foreach ($resutl->result() as $item_c) {
                    $data['nombre']=$item_c->nombre;
                    $data['status']=$item_c->status;
                    $data['motivo']=$item_c->motivo;
                    $idpersonal_inicio=$item_c->idpersonal_inicio;
                    if($item_c->departamento==1){ $tipo_dep='Ventas';}
                    if($item_c->departamento==2){ $tipo_dep='Contratos / Arrendamiento';}
                    if($item_c->departamento==3){ $tipo_dep='Pólizas';}
                    if($item_c->departamento==4){ $tipo_dep='Servicio Técnico';}
                }
                if($idpersonal>0){
                    $resutl_eje = $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$idpersonal));
                    foreach ($resutl_eje->result() as $item_e) {
                        $data['nombre']=$item_e->nombre;
                    }
                }
                $data['tipo_dep']=$tipo_dep;
                if($idpersonal_inicio>0){
                    $resutl_eje = $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$idpersonal_inicio));
                    foreach ($resutl_eje->result() as $item_e) {
                        $data['ejecutivo']=$item_e->nombre;
                    }
                }else{
                    $data['ejecutivo']='';
                }
            //======================================
            $this->load->view('chat2',$data);
        }
    }
    function consultastatus(){
        $params = $this->input->post();
        $idc = $params['idc'];
        $resutl = $this->ModeloCatalogos->getselectwheren('chat_ini',array('id'=>$idc));
        $status =0;
        foreach ($resutl->result() as $item_c) {
            $status=$item_c->status;    
        }

        echo $status; 

    }
    function consulta_msn(){
        $params = $this->input->post();
        $idc = $params['idc'];
        $resutl = $this->ModeloCatalogos->getselectwheren('chat_ini_msn',array('id_chat'=>$idc));
        $res_c = $this->ModeloCatalogos->getselectwheren('chat_ini',array('id'=>$idc));
        
        $html ='';$status=1;
        foreach ($res_c->result() as $it_c) {
            $idpersonal_inicio = $it_c->idpersonal_inicio;
            $status = $it_c->status;
            if($status==2){
                $html.='<script type="text/javascript">';
                $html.="$('body').loading({theme: 'dark',message: 'Chat con ejecutivo cerrado, gracias por contactarse con nosotros'});";
                $html.="setInterval(function () {";
                    $html.='location.href ="https://kyoceraap.com/index.php/Icha?ttp=0"';
                $html.='}, 3000);';
                $html.='</script>';
            }
            
            if($it_c->motivo!=''){
                $html.='<div class="group_message envio_0">';
                    $html.='<div class="tit">Motivo del contacto</div>';
                    $html.='<div class="content">'.$it_c->motivo.'</div>';
                $html.='</div>';
            }
            if($idpersonal_inicio>0){
                $resutl_eje = $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$idpersonal_inicio));
                foreach ($resutl_eje->result() as $item_e) {
                    $nombre_eje=$item_e->nombre;
                }
                $html.='<div class="group_message envio_1">';
                    $html.='<div class="tit">'.$nombre_eje.'</div>';
                    $html.='<div class="content">Gracias por contactar a Alta Productividad, le atiende '.$nombre_eje.' ¿Cómo le podemos ayudar hoy?</div>';
                $html.='</div>';
            }
        }
        
        foreach ($resutl->result() as $item_c) {
            if($item_c->idpersonal>0){
                $envio=1;
            }else{
                $envio=0;
            }
            $html.='<div class="group_message envio_'.$envio.'">';
                $html.='<div class="tit">'.$item_c->name.'</div>';
                $html.='<div class="content">'.$item_c->contenido.'</div>';
                $html.='<div class="inf_ch">'.date("H:m:s", strtotime($item_c->reg)).'</div>';
              $html.='</div>';
        }
        echo $html;
    }
    function save_message(){
        $params = $this->input->post();
        $idc = $params['idc'];
        $user = $params['user'];
        $idp = $params['idp'];
        $mess = $params['mess'];

        $this->ModeloCatalogos->Insert('chat_ini_msn',array('id_chat'=>$idc,'name'=>$user,'contenido'=>$mess,'idpersonal'=>$idp));
    }
    
}