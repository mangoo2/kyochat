<html class="no-js" lang="en"><!--<![endif]--><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Kyo chat</title>

  <meta content="width=device-width,initial-scale=1.0" name="viewport">


  <script src="<?php echo base_url();?>client/lib/jquery-1.8.2.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>client/themes/default/jquery.phpfreechat.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/style_chat.css?v=<?php echo date('YmdHis');?>">
  <script src="<?php echo base_url();?>client/jquery.phpfreechat.min.js" type="text/javascript"></script>  
  <link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
  <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
  <link href="<?php echo base_url(); ?>public/plugins/note/summernote.min.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>public/plugins/note/summernote.min.js"></script>
  <style type="text/css">
    .envio_<?php echo $ms_per;?>{
      margin-left: auto;
    }
  </style>
</head>
<body>
  <header>

  </header>
  <div role="main">
    <input type="hidden" id="base_url" value="<?php echo base_url();?>">

    <?php
      if($status==0){
        ?>
          <div class="row" style="display: none;">
            <div class="col-md-12" style="text-align: center;">
              <!--Numero de cliente: <b><?php echo $idcliente;?></b><br>-->
              Departamento <?php echo $tipo_dep; ?>
            </div>
          </div>
          <div class="row" style="display: none;">
            <div class="col-md-12 chat_select" style="color:red; text-align: center;">
              <img src="<?php echo base_url()?>public/img/ring.gif" class="loading">
            </div>
            <div class="col-md-12" style="color:red; text-align: center;">
              Conectando con algún ejecutivo, por favor espera..
            </div>
          </div>
        <?php
      }
      
      if($status==1){
        $html='';
          //======================================
            $html.='<div class="pfc-messages" id="pfc-messages">';
            $html.='</div>';
            $html.='<div class="pfc-compose" style="text-align: center;">';
                $html.='<textarea data-to="channel|xxx" id="content_message" class="form-control summernote" data-idc="'.$idc.'" data-user="'.$nombre.'" data-idp="'.$idpersonal.'"></textarea>';
                if($ms_per==1){
                  $url_f='https://altaproductividadapr.com/index.php/Rchat/finalizar/'.$idc;
                  $html.='<a type="button" class="btn btn-danger" href="'.$url_f.'">Finalizar chat</a>';
                }
            $html.='</div>';
            /*
            $html.='<div class="pfc-hook">';
              $html.='<div class="pfc-content pfc-notabs">';
                $html.='<div class="pfc-tabs">';
                  $html.='<ul></ul>';
                $html.='</div>';
                $html.='<div class="pfc-topic">';
                  $html.='<a class="pfc-toggle-tabs"></a>';
                  $html.='<p><span class="pfc-topic-label">Topic:</span> <span class="pfc-topic-value">no topic for this channel</span></p>';
                  $html.='<a class="pfc-toggle-users"></a>';
                $html.='</div>';
                $html.='<div class="pfc-messages" id="pfc-messages">';
                  /*
                  $html.='<div class="pfc-message-mobile-padding"></div>';
                  $html.='<div class="messages-group from-system-message" data-stamp="1720131061" data-from="system-message">';
                    $html.='<div class="date">16:11:01</div>';
                    $html.='<div class="message">gerardo joined the channel</div>';
                  $html.='</div>';
                  $html.='<div class="messages-group" data-stamp="1720131325" data-from="ce0b0f4497c967099c00c2a95f5c3dea79c0a04e">';
                    $html.='<div class="date">16:15:25</div>';
                    $html.='<div class="name">gerardo</div>';
                    $html.='<div class="message">sdfsdfsdfsd</div>';
                    $html.='<div class="message">sdfsdfsdf</div>';
                  $html.='</div>';
                  */
                  /*
                $html.='</div>';
                $html.='<div class="pfc-users" style="width: 193px;"><!--aqui es donde se define si es visible o no-->';
                  $html.='<div class="pfc-role-admin">';
                    $html.='<p class="role-title" style="display: block;">Solicitante</p>';
                    $html.='<ul>';
                      $html.='<li class="user first" id="user_ce0b0f4497c967099c00c2a95f5c3dea79c0a04e">';
                        $html.='<div class="status st-active st-op"></div>';
                        $html.='<div class="name">'.$nombre.'</div>';
                        $html.='<div class="avatar"></div>';
                      $html.='</li>';
                    $html.='</ul>';
                  $html.='</div>';
                  $html.='<div class="pfc-role-user">';
                    $html.='<p class="role-title" style="display: block;">Ejecutivo</p>';
                    $html.='<ul>';
                      $html.='<li class="user first" id="user_1a9bc85bb60670e5f25a0e808d04214682f1dc44">';
                        $html.='<div class="status st-active"></div>';
                        $html.='<div class="name">dddd</div>';
                        $html.='<div class="avatar"></div>';
                      $html.='</li></ul>';
                    $html.='</div>';
                  $html.='</div>';
                  $html.='<div class="pfc-footer">';
                    
                  $html.='</div>';
                  $html.='<div class="pfc-compose">';
                    $html.='<textarea data-to="channel|xxx" id="content_message" style="width: 1869px;" data-idc="'.$idc.'" data-user="'.$nombre.'" data-idp="'.$idpersonal.'"></textarea>';
                  $html.='</div>';
                  $html.='<div class="pfc-modal-overlay" style=""></div>';
                  $html.='<div class="pfc-modal-box" style="left: 933.5px; top: 243px;"></div>';
                  $html.='<div class="pfc-ad-desktop"></div>';
                  $html.='<div class="pfc-ad-tablet"></div>';
                  $html.='<div class="pfc-ad-mobile"></div>';
                $html.='</div>';
              $html.='</div>';
              */
              echo $html;
          //======================================
      }
      
    ?>
    
    
  </div>
  <footer>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
    <script src="<?php echo base_url();?>public/js/script.js?v=<?php echo date('YmdHis') ?>" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function($) {
        <?php if($status==2){ ?>
          location.href ="https://kyoceraap.com/index.php/Icha?ses=1&ttp=1";   
        <?php } ?>
        <?php if($status==0){ ?>
          $('body').loading({theme: 'dark',message: 'Conectando con un asesor, por favor espera…'});
          setInterval(function () {
            consultastatus(<?php echo $idc;?>);
          }, 2000);
          
        <?php } ?>
        <?php if($status==1){ ?>
          setInterval(function () {
            consulta_msn(<?php echo $idc.','.$ms_cli.','.$ms_per;?>);
          }, 3000);
          
        <?php } ?>

      });
    </script>
  </footer>

</body>

<div id="custom-overlay" class="loading-hidden" style="position: absolute; z-index: 1; top: 2609.38px; left: 381.5px; width: 360px; height: 180px; display: none;">
    <div class="row info_lading">
      <div class="col-md-12">
        Asesor conectado
      </div>
      <div class="col-md-12">
        <img src="<?php echo base_url();?>public/img/check.svg" class="img_checl">
      </div>
    </div>
      <!--<div class="loading-spinner">
        Custom loading...
      </div>-->
</div>
</html>