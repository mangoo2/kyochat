<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Kyo chat</title>

  <meta content="width=device-width,initial-scale=1.0" name=viewport>


  <script src="<?php echo base_url();?>client/lib/jquery-1.8.2.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>client/themes/default/jquery.phpfreechat.min.css" />
  <script src="<?php echo base_url();?>client/jquery.phpfreechat.min.js" type="text/javascript"></script>

  <style type="text/css">
    .pfc-hook{
      margin-top: 35px;
    }
  </style>
  
</head>
<body>
  <header>

  </header>
  <div role="main">


    
    <div class="pfc-hook"><a href="http://www.phpfreechat.net">Creating chat rooms everywhere - phpFreeChat</a></div>
    <script type="text/javascript">
      // Obtener los nombres de los usuarios desde el servidor (por ejemplo, mediante PHP)
      var user1 = "<?php if(isset($_GET['get1'])){ echo $_GET['get1'];}else{ echo 'usu1';} ?>";
      var user2 = "<?php if(isset($_GET['get2'])){ echo $_GET['get2'];}else{ echo 'usu2';} ?>";
      
      // Crear un ID único para la sala basado en los nombres de usuario
      var room_id = 'room_' + user1 + '_' + user2;

      // Inicializar el chat con configuración personalizada
      console.log(room_id);
      $('.pfc-hook').phpfreechat({
        room: room_id,
        nick: user1,
        title: 'Chat entre ' + user1 + ' y ' + user2,
        maxUsers: 2 // Limitar a dos usuarios
      });
    </script>

  </div>
  <footer>

  </footer>
</body>
</html>